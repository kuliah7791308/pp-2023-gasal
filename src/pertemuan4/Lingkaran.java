package pertemuan4;

import java.util.Scanner;

public class Lingkaran {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        float PI = 22f / 7f;
        System.out.print("Masukkan r = ");
        float r = s.nextFloat();
        float luas = PI * r * r;
        float keliling = PI * 2 * r;
        System.out.println("Luas : " + luas);
        System.out.println("Keliling : " + keliling);
    }
}

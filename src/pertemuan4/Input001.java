package pertemuan4;

import java.util.Scanner;

public class Input001 {

    public static void main(String[] args) {
        //Class Scanner
        Scanner s = new Scanner(System.in);
        System.out.print("Masukkan nama anda : ");
        String nama = s.nextLine();
        System.out.print("Masukkan umur anda : ");
        int umur = s.nextInt();

        System.out.println("Nama yang anda inputkan adalah : " + nama);
        System.out.println("Umur yang anda inputkan adalah : " + umur);
    }
}

package pertemuan4;

import java.util.Scanner;

public class Balok {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.print("Panjang : ");
        int p = s.nextInt();
        System.out.print("Lebar : ");
        int l = s.nextInt();
        System.out.print("Tinggi : ");
        int t = s.nextInt();
        int volume = p * l * t;
        int luas = 2 * (p * l + p * t + l * t);
        System.out.println("Volume = " + volume);
        System.out.println("Luas Permukaan  = " + luas);
    }
}

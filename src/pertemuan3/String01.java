package pertemuan3;

public class String01 {

    public static void main(String[] args) {
        String s = "  hello World  ";
        //menghilangkan whitespace diawal dan akhir
        String hasilTrim = s.trim();
        System.out.println(s);
        System.out.println(hasilTrim);
        //panjanga string, gunakan fungsi length()
        System.out.println("Panjang S = "+s.length());
        System.out.println("Panjang hT = "+hasilTrim.length());
      
    }
}

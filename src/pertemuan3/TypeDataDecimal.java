package pertemuan3;

public class TypeDataDecimal {

    public static void main(String[] args) {
        //tipe data float
        float a = 22f / 7f;
        //tipe data double
        double b = 22d / 7d;
        System.out.println("Hasil PI dengan Float = "+a);
        System.out.println("Hasil PI dengan Double = "+b);
    }
}

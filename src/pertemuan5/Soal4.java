package pertemuan5;

import java.util.Scanner;

public class Soal4 {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.print("Masukkan sebuah angka : ");
        int a = s.nextInt();
        if (a >= 1 && a <= 100) {
            System.out.println("Anda dalam rentang");
        } else {
            System.out.println("Anda di luar rentang 1 - 100");
        }
    }
}

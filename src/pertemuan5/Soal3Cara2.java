package pertemuan5;

import java.util.Scanner;

public class Soal3Cara2 {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.print("Masukkan bilangan I = ");
        int a = s.nextInt();
        System.out.print("Masukkan bilangan II = ");
        int b = s.nextInt();
        System.out.print("Pilih salah satu (+, -, *, /) : ");
        String operator = s.next();
        if (operator.equals("+")) {
            System.out.println("Hasil Jumlah = " + (a + b));
        } else if (operator.equals("-")) {
            System.out.println("Hasil Penguranang= = " + (a - b));
        } else if (operator.equals("*")) {
            System.out.println("Hasil Perkalian = " + (a * b));
        } else if (operator.equals("/")) {
            System.out.println("Hasil Pembagian= = " + ((float) a / (float) b));
        } else {
            System.out.println("Silahkan pilih (+, -, *, /)");
        }
    }
}

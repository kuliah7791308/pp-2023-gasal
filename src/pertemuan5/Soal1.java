package pertemuan5;

import java.util.Scanner;

public class Soal1 {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.print("Masukkan sebuah bilangan = ");
        int a = s.nextInt();
        if (a % 2 == 0) {
            System.out.println("Bilangan Genap");
        } else {
            System.out.println("Bilangan Ganjil");
        }

    }
}

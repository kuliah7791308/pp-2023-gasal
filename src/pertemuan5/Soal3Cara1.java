package pertemuan5;

import java.util.Scanner;

public class Soal3Cara1 {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.print("Masukkan bilangan I = ");
        int a = s.nextInt();
        System.out.print("Masukkan bilangan II = ");
        int b = s.nextInt();
        System.out.println("1 untuk +");
        System.out.println("2 untuk -");
        System.out.println("3 untuk *");
        System.out.println("4 untuk /");
        System.out.print("Pilih salah satu : ");
        String operator = s.next();
        if (operator.equals("1")) {
            System.out.println("Hasil Jumlah = " + (a + b));
        } else if (operator.equals("2")) {
            System.out.println("Hasil Penguranang = " + (a - b));
        } else if (operator.equals("3")) {
            System.out.println("Hasil Perkalian = " + (a * b));
        } else if (operator.equals("4")) {
            System.out.println("Hasil Pembagian= = " + ((float) a / (float) b));
        } else {
            System.out.println("Silahkan pilih 1 - 4");
        }
    }
}

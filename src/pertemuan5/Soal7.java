package pertemuan5;

import java.util.Scanner;

public class Soal7 {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.print("Masukkan usia = ");
        int usia = s.nextInt();

        if (usia >= 0 && usia <= 11) {
            System.out.println("Anak - anak");
        } else if (usia >= 12 && usia <= 17) {
            System.out.println("Remaja");
        } else if (usia >= 18 && usia <= 60) {
            System.out.println("Dewasa");
        } else if (usia >= 61) {
            System.out.println("Lanjut Usia");
        } else {
            System.out.println("Umur tidak boleh di bawah 0");
        }
    }
}

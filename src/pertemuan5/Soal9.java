package pertemuan5;

import java.util.Scanner;

public class Soal9 {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.print("Masukkan suhu dalam Celcius = ");
        float c = s.nextFloat();
//        F = (9/5 x °C) + 32.
        float f = (9f / 5f * c) + 32;
        System.out.println("Suhu dalam Fahrenheit = " + f);
    }
}

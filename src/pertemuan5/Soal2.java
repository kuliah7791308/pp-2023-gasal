package pertemuan5;

import java.util.Scanner;

public class Soal2 {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.print("Masukkan bilangan I = ");
        int a = s.nextInt();
        System.out.print("Masukkan bilangan II = ");
        int b = s.nextInt();
        if (a > b) {
            System.out.println(a + " lebih besar dari " + b);
        } else if (a < b) {
            System.out.println(a + " lebih kecil dari " + b);
        } else{
            System.out.println(a + " sama dengan " + b);
        }
    }
}

package pertemuan2;

public class Varibel02 {

    public static void main(String[] args) {
        int bilangan1 = 34;
        int bilangan2 = 30;
        int hasilJumlah = bilangan1 + bilangan2;
        int hasilPengurangan = bilangan1 - bilangan2;
        int hasilPerkalian = bilangan1 * bilangan2;
        float hasilBagi = (float) bilangan1 / (float) bilangan2;
        System.out.println("Hasil Jumlah = " + hasilJumlah);
        System.out.println("Hasil Pengurangan = " + hasilPengurangan);
        System.out.println("Hasil Kali = " + hasilPerkalian);
        System.out.println("Hasil Bagi = " + hasilBagi);
    }
}
/**
 * Penjumlahan + Perkalian * Pembagian / Pengurangan - | Modulo %
 */

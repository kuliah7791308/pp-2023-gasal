package pertemuan2;

import java.util.Scanner;

/**
 * This file is belong to Universitas Kristen Immanuel
 *
 * @author Yoel Pieter S.
 */
public class Testin {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.print("Masukkan nama = ");
        String nama = s.next();
        System.out.println("Nama yang anda masukkan = " + nama);
    }
}
